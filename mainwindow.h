#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <gst/gst.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    GstElement *m_videoSink1;
    GstElement *m_videoSink2;
    GstElement *m_pipeline;
    WId m_winId1;
    WId m_winId2;
};
#endif // MAINWINDOW_H
